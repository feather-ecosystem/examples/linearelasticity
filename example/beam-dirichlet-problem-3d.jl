using AbstractMappings, IgaBase, SortedSequences, UnivariateSplines, TensorProductBsplines
using CartesianProducts, StaticArrays

using LinearElasticity

# model description
dom = Interval(0.0,1.0) ⨱ Interval(0.0,1.0) ⨱ Interval(0.0,1.0)
mapping = GeometricMapping(dom, (x,y,z) -> 3x, (x,y,z) -> y, (x,y,z) -> z)
material = Material(dim=3, youngs_modulus=1e5, poisson_ratio=0.3)

# stress field
function Σ(x,y,z)
    if x ≈ 3.0
        return @SMatrix [0.0 0.0 10.0; 0.0 0.0 0.0; 10.0 0.0 0.0]
    end
    return @SMatrix [0.0 0.0 0.0; 0.0 0.0 0.0; 0.0 0.0 0.0]
end

# partitioning into elements
partition = CartesianProduct((d,n) -> IncreasingRange(d,n), domain(mapping), (21,5,5));

# spline discretization
trialspace = TensorProduct(u -> SplineSpace(2, u), partition);

# displacement field
displacement = GeometricMapping(TensorProductBspline, trialspace; codimension=3);

# solve problem
fixed_nodes_x = @view displacement[1].indices[1,:,:];
fixed_nodes_y = @view displacement[2].indices[1,:,:];
fixed_nodes_z = @view displacement[3].indices[1,:,:];
fixed_nodes = (fixed_nodes_x, fixed_nodes_y, fixed_nodes_z);
solve!(fixed_nodes=fixed_nodes, displacement=displacement, mapping=mapping, material=material, traction=Σ);

# stress object
stress = CauchyStress(mapping, displacement, material);

# plot result
using GLMakie
GLMakie.activate!()

fig = Figure(resolution = (1200, 800))
ax = LScene(fig[1, 1], show_axis = true)

# evaluation grid
X = CartesianProduct((d,m) -> IncreasingRange(d,m), dom, (51,21,21));

for side in 1:6
    
    xs = restrict_to(X, side=side)
    @evaluate Y = mapping(xs)
    @evaluate Δu = displacement(xs)
    @evaluate S = stress(xs)
    Y .+= 20Δu;

    x, y, z = map(a -> Matrix(squeeze(a)), Y.data)
    s = Matrix(squeeze(S.data[1,1]))
    
    GLMakie.surface!(ax, x, y, z; color = s, colormap = :jet, colorrange=(-250, 250))
end