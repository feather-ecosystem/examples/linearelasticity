using AbstractMappings, SortedSequences, UnivariateSplines, TensorProductBsplines
using CartesianProducts, StaticArrays, LinearAlgebra

using LinearElasticity, Test

# model description
mapping = GeometricMapping(Interval(0.0,1.0) ⨱ Interval(0.0,1.0), (x,y) -> 3x, (x,y) -> y);
material = Material(dim=2, youngs_modulus=1e5, poisson_ratio=0.3)

# stress field
Σ(x,y) = @SMatrix [10.0 0.0; 0.0 0.0]

# partitioning into elements
partition = CartesianProduct((d,n) -> IncreasingRange(d,n), domain(mapping), (5,2))

# spline discretization
trialspace = TensorProduct(
                SplineSpace(2, partition.data[1]), # clamped b.c. at left boundary
                SplineSpace(2, partition.data[2])
            )
displacement = GeometricMapping(TensorProductBspline, trialspace; codimension=2);

# solve problem
fixed_nodes_x = @view displacement[1].indices[1,:];
fixed_nodes_y = @view displacement[2].indices[1,1];
fixed_nodes = (fixed_nodes_x, fixed_nodes_y);
solve!(fixed_nodes=fixed_nodes, displacement=displacement, mapping=mapping, material=material, traction=Σ);

# evaluate the mapping
stress = CauchyStress(mapping, displacement, material);

# patch test
σ = Field{2,2}((x,y)->Σ(x,y)[1,1], (x,y)->Σ(x,y)[2,1], (x,y)->Σ(x,y)[1,2], (x,y)->Σ(x,y)[2,2])

# compue L2 error in displacement and stress
@test norm(l2_error(stress, to=σ ∘ mapping)) < 1e-12
