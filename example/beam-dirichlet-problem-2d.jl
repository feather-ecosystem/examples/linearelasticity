using AbstractMappings, SortedSequences, UnivariateSplines, TensorProductBsplines
using CartesianProducts, StaticArrays

using LinearElasticity

# model description
mapping = GeometricMapping(Interval(0.0,1.0) ⨱ Interval(0.0,1.0), (x,y) -> 3x, (x,y) -> y)
material = Material(dim=2, youngs_modulus=1e5, poisson_ratio=0.3)

# stress field
function Σ(x,y)
    if x ≈ 3.0
        return @SMatrix [0.0 10.0; 10.0 0.0]
    end
    return @SMatrix [0.0 0.0; 0.0 0.0]
end

# partitioning into elements
partition = CartesianProduct((d,n) -> IncreasingRange(d,n), domain(mapping), (5,2))

# spline discretization
trialspace = TensorProduct(
                SplineSpace(2, partition.data[1]), # clamped b.c. at left boundary
                SplineSpace(2, partition.data[2])
            )
displacement = GeometricMapping(TensorProductBspline, trialspace; codimension=2);

# solve problem
fixed_nodes_x = @view displacement[1].indices[1, :]
fixed_nodes_y = @view displacement[2].indices[1,1]
fixed_nodes = (fixed_nodes_x, fixed_nodes_y)
solve!(fixed_nodes=fixed_nodes, displacement=displacement, mapping=mapping, material=material, traction=Σ);

# stress object
stress = CauchyStress(mapping, displacement, material);

# plot result
using GLMakie
GLMakie.activate!()

fig = Figure(resolution = (1200, 800))
ax = LScene(fig[1, 1], show_axis = true)

# get visualization points in parametric space
x = visualization_grid(mapping, density=(21,11));

@evaluate Y = mapping(x)
@evaluate Δu = displacement(x)
@evaluate S = stress(x)
Y .+= 10Δu;

x, y = map(Matrix, Y.data)
z = zeros(size(x))
s = Matrix(S.data[1,1])

GLMakie.surface!(ax, x, y, z; color = s, colormap = :jet, colorrange=(-250, 250))