using AbstractMappings, SortedSequences, UnivariateSplines, TensorProductBsplines
using CartesianProducts, StaticArrays, LinearAlgebra

using LinearElasticity, Test

# model description
dom = Interval(0.0,1.0) ⨱ Interval(0.0,1.0) ⨱ Interval(0.0,1.0)
mapping = GeometricMapping(dom, (x,y,z) -> 3x, (x,y,z) -> y, (x,y,z) -> z)
material = Material(dim=3, youngs_modulus=1e5, poisson_ratio=0.3)

# stress field
Σ(x,y,z) = @SMatrix [10.0 0.0 0.0; 0.0 0.0 0.0; 0.0 0.0 0.0]

# partitioning into elements
partition = CartesianProduct((d,n) -> IncreasingRange(d,n), domain(mapping), (5,5,5));

# spline discretization
trialspace = TensorProduct(u -> SplineSpace(2, u), partition);

# displacement field
displacement = GeometricMapping(TensorProductBspline, trialspace; codimension=3);

# solve problem
fixed_nodes_x = @view displacement[1].indices[1,:,:];
fixed_nodes_y = @view displacement[2].indices[1,1,1];
fixed_nodes_z = @view displacement[3].indices[1,1,1];
fixed_nodes = (fixed_nodes_x, fixed_nodes_y, fixed_nodes_z);
solve!(fixed_nodes=fixed_nodes, displacement=displacement, mapping=mapping, material=material, traction=Σ)

# stress object
stress = CauchyStress(mapping, displacement, material);

# analytical stress field
σ = Field{3,3}(
        (x,y,z)->Σ(x,y,z)[1,1], (x,y,z)->Σ(x,y,z)[2,1], (x,y,z)->Σ(x,y,z)[3,1], 
        (x,y,z)->Σ(x,y,z)[1,2], (x,y,z)->Σ(x,y,z)[2,2], (x,y,z)->Σ(x,y,z)[3,2], 
        (x,y,z)->Σ(x,y,z)[1,3], (x,y,z)->Σ(x,y,z)[2,3], (x,y,z)->Σ(x,y,z)[3,3])

# compue L2 error in displacement and stress
@test  norm(l2_error(stress, to=σ ∘ mapping)) < 1e-12