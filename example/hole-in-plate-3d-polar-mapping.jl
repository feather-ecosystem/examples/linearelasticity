using IgaBase, AbstractMappings, SortedSequences, UnivariateSplines, TensorProductBsplines
using CartesianProducts, StaticArrays

using LinearElasticity

# model description
dom = Interval(1.0,4.0) ⨱ Interval(0.0,pi/2) ⨱ Interval(0.0,1.0)
mapping = GeometricMapping(dom, (r,θ,z) -> r*cos(θ), (r,θ,z) -> r*sin(θ), (r,θ,z) -> z)
material = Material(dim=3, youngs_modulus=1e5, poisson_ratio=0.3)

# get analytical solutions
benchmark = benchmark_hole_in_plate_3d(material=material);

# partitioning into elements
partition = CartesianProduct((d,n) -> IncreasingRange(d,n), domain(mapping), (11,21,3));

# spline discretization
trialspace = TensorProduct(u -> SplineSpace(4, u), partition);

# displacement field
displacement = GeometricMapping(TensorProductBspline, trialspace; codimension=3);

# solve problem
fixed_nodes_x = displacement[1].indices[:,end,:];
fixed_nodes_y = displacement[2].indices[:,1,:];
fixed_nodes_z = [displacement[3].indices[:,:,1], displacement[3].indices[:,:,end]]; # plain strain condition
fixed_nodes = (fixed_nodes_x, fixed_nodes_y, fixed_nodes_z);
solve!(fixed_nodes=fixed_nodes, displacement=displacement, mapping=mapping, material=material, traction=benchmark.stress)

# get the stresses
stress = CauchyStress(mapping, displacement, material);

# plot result
using GLMakie
GLMakie.activate!()

fig = Figure(resolution = (1200, 800))
ax = LScene(fig[1, 1], show_axis = true)

# get visualization points in parametric space
X = CartesianProduct((u,m) -> IncreasingRange(u, m), dom, (201, 101, 10));

for dir in 1:3
    for comp in 1:2

        side = comp + (dir-1)*2

        # get the evalation points at this boundary
        x = restrict_to(X, side=side)

        # evaluate coordinates, displacement, and stress
        @evaluate Y = mapping(x);
        @evaluate Δu = displacement(x);
        @evaluate S = stress(x);

        # plot deformed configuration
        Y .+= 2000Δu;

        x, y, z = map(squeeze, Y.data);
        s = squeeze(S.data[1,1]);
        if comp==1 # invert normal such that Makie does shading on correct side
            x = x'; y = y'; z = z'; s = s'
        end
        GLMakie.surface!(ax, x, y, z; color = s, colormap = :jet, colorrange=(-10,30))        
        GLMakie.surface!(ax, -x', y', z'; color = s', colormap = :jet, colorrange=(-10, 30))
        GLMakie.surface!(ax, -x, -y, z; color = s, colormap = :jet, colorrange=(-10, 30))
        GLMakie.surface!(ax, x', -y', z'; color = s', colormap = :jet, colorrange=(-10, 30))
    end
end

# get analytical displacement and strain. These still need to be evaluated as scalar functions.
u = GeometricMapping(Interval(0.0,4.0) ⨱ Interval(0.0,4.0) ⨱ Interval(0.0,1.0),
        (x,y,z)->benchmark.displacement(x,y,z)[1],
        (x,y,z)->benchmark.displacement(x,y,z)[2], 
        (x,y,z)->benchmark.displacement(x,y,z)[3])

σ = Field{3,3}(
        (x,y,z)->benchmark.stress(x,y,z)[1,1], 
        (x,y,z)->benchmark.stress(x,y,z)[2,1], 
        (x,y,z)->benchmark.stress(x,y,z)[3,1], 
        (x,y,z)->benchmark.stress(x,y,z)[1,2], 
        (x,y,z)->benchmark.stress(x,y,z)[2,2], 
        (x,y,z)->benchmark.stress(x,y,z)[3,2], 
        (x,y,z)->benchmark.stress(x,y,z)[1,3], 
        (x,y,z)->benchmark.stress(x,y,z)[2,3], 
        (x,y,z)->benchmark.stress(x,y,z)[3,3])

# compue L2 error in displacement and stress
e_u = l2_error(displacement, to=u ∘ mapping, relative=false)
e_σ = l2_error(stress, to=σ ∘ mapping, relative=false)