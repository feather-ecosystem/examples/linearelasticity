using AbstractMappings, SortedSequences, UnivariateSplines, TensorProductBsplines, NURBS
using CartesianProducts, StaticArrays

using LinearElasticity

# model initialization
mapping = NURBS.hole_in_square_plate();
material = Material(dim=2, youngs_modulus=1e5, poisson_ratio=0.3)

# get analytical solutions
benchmark = benchmark_hole_in_plate_2d(material=material);

# spline discretization
trialspace = TensorProduct(s -> refine(s, kRefinement(10,4)), mapping.space)

# displacement field
displacement = GeometricMapping(TensorProductBspline, trialspace; codimension=2);

# solve problem
fixed_nodes_x = @view displacement[1].indices[1,:];
fixed_nodes_y = @view displacement[2].indices[end,:];
fixed_nodes = (fixed_nodes_x, fixed_nodes_y);
solve!(fixed_nodes=fixed_nodes, displacement=displacement, mapping=mapping, material=material, traction=benchmark.stress)

# get the stresses
stress = CauchyStress(mapping, displacement, material);

# plot result
using GLMakie
GLMakie.activate!()

fig = Figure(resolution = (1200, 800))
ax = LScene(fig[1, 1], show_axis = true)

# get visualization points in parametric space
x = visualization_grid(mapping, density=(201,101));

# evaluate coordinates, displacement, and stress
@evaluate Y = mapping(x);
@evaluate Δu = displacement(x);
@evaluate S = stress(x);

# plot deformed configuration
Y .+= 2000Δu;

x, y = map(Matrix, Y.data);
z = zeros(size(x));
s = Matrix(S.data[1,1]);
GLMakie.surface!(ax, x, y, z; color = s, colormap = :jet, colorrange=(-10,30))
GLMakie.surface!(ax, -x', y', z'; color = s', colormap = :jet, colorrange=(-10, 30))
GLMakie.surface!(ax, -x, -y, z; color = s, colormap = :jet, colorrange=(-10, 30))
GLMakie.surface!(ax, x', -y', z'; color = s', colormap = :jet, colorrange=(-10, 30))

# get analytical displacement and strain. These still need to be evaluated as scalar functions.
u = GeometricMapping(Interval(0.0,4.0) ⨱ Interval(0.0,4.0), (x,y)->benchmark.displacement(x,y)[1], (x,y)->benchmark.displacement(x,y)[2])
σ = Field{2,2}((x,y)->benchmark.stress(x,y)[1,1], (x,y)->benchmark.stress(x,y)[2,1], (x,y)->benchmark.stress(x,y)[1,2], (x,y)->benchmark.stress(x,y)[2,2])

# compue L2 error in displacement and stress
l2_error(displacement, to=u ∘ mapping, relative=true)
l2_error(stress, to=σ ∘ mapping, relative=true)