export CauchyStress

struct CauchyStress{Dim,T1<:GeometricMapping{Dim}, T2<:GeometricMapping{Dim}} <: AbstractMapping{Dim,Dim,Dim}
    mapping::T1
    deformation::T2
    material::Material{Dim}
    function CauchyStress(mapping::GeometricMapping{Dim}, deformation::GeometricMapping{Dim}, material::Material{Dim}) where Dim
        T1 = typeof(mapping)
        T2 = typeof(deformation)
        new{Dim,T1,T2}(mapping, deformation, material)
    end
end

IgaBase.standard_quadrature_rule(f, g::CauchyStress) = IgaBase.standard_quadrature_rule(f, g.deformation)

for Dim in 2:3

    # enables fast evaluation of stress

    @eval function AbstractMappings.evalkernel!(::Val{:(=)}, y::EvaluationSet{$Dim,$Dim}, x::CartesianProduct{$Dim}, σ::CauchyStress{$Dim})

        # initialize
        mapping = σ.mapping
        displacement = σ.deformation
        material = σ.material  

        # get the material properties
        E = material.youngs_modulus
        ν = material.poisson_ratio
        μ = E / (2.0 * (1.0 + ν))
        λ = ν * E / ((1.0 + ν) * (1.0-2.0ν))
        
        # identity matrix
        Id = SMatrix{$Dim,$Dim}(I)

        # evaluate jacobian of the mapping at all points
        # and the gradient of displacement
        @evaluate ∇F = Gradient(mapping)(x)
        @evaluate! y = Gradient(displacement)(x)
        
        # pull back transformation at each quadrature point
        for k in 1:length(x)  
            J = ∇F[k]
            ∇u = inv(J) * y[k] 
            ε = 0.5 * (∇u + ∇u')
            y[k] = λ * tr(ε) * Id + 2.0 * μ * ε        
        end
        return y
    end
end