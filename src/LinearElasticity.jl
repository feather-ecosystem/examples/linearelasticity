module LinearElasticity

using IgaBase, AbstractMappings
using SortedSequences, UnivariateSplines, TensorProductBsplines, NURBS
using CartesianProducts, KroneckerProducts
using IgaFormation

using LinearAlgebra, SparseArrays, StaticArrays

include("constitutive.jl")
include("assemble.jl")
include("benchmarks.jl")
include("solve.jl")
include("postprocessing.jl")

end # module
