export solve!

function solve!(; fixed_nodes, displacement, mapping::GeometricMapping, material::Material, traction)

    # initialize spline space
    space = displacement[1].space

    # extract partition from the solution space
    partition = CartesianProduct(s -> breakpoints(s), displacement[1].space)

    # define regular quadrature rule, element accessor, and element sum-factorization cache
    quadrule = TensorProduct((d, u) -> PatchRule(d; npoints=ceil(Int, Degree(u)+1), method=Legendre), partition, space);

    # assemble stiffness matrix and forcing vector
    K, F = assemble(fixed_nodes=fixed_nodes, displacement=displacement, quadrule=quadrule, mapping=mapping, material=material, traction=traction)

    # solve final system of equations
    u = K \ F

    # create discrete solution field
    set_solution_field!(displacement, u)
end

# prescribe the solution
function set_solution_field!(solution, u)
    n = dimension(solution)
    m = Int(length(u) / n)
    for i in 1:n
        A = (i-1)*m+1:i*m
        solution[i].coeffs[:] .= u[A]
    end
end