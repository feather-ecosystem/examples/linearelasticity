export assemble

function assemble(; fixed_nodes, displacement::GeometricMapping{Dim}, quadrule, mapping::GeometricMapping{Dim}, material::Material{Dim}, traction) where Dim

    space = displacement[1].space

    # extract partition from the solution space
    partition = CartesianProduct(s -> breakpoints(s), space)

    # create an element accessor
    acc = ElementAccessor(testspace=space, trialspace=space, quadrule=quadrule, incorporate_weights_in_testfuns=true);

    # create cache for sum factorization
    mat_cache = MatrixSumfactoryCache(acc);
    vec_cache = VectorSumfactoryCache(acc);

    # compute the material matrix
    pullback_c!(e) = pullback_material_law!(acc, e, [FieldEvaluationCache(acc, Dim, Dim) for i in 1:Dim, j in 1:Dim], material, mapping)

    # allocate space for stiffness matrices and forcing vectors
    m = prod(s -> dimsplinespace(s), space)
    K = [spzeros(m, m) for i in 1:Dim, j in 1:Dim]
    f = [zeros(m) for i in 1:Dim]

    # loop over elements
    for element in Elements(partition)

        # get global indices
        A = TestIndices(acc, element)
        B = TrialIndices(acc, element)

        # compute trial- and test-function derivatives at the quadrature points
        ∇u = map(k -> TrialFunctions(acc, element; ders=ders(Dim, k)), 1:Dim)
        ∇v = map(k -> TestFunctions(acc, element; ders=ders(Dim, k)), 1:Dim)

        # compute the pulled-back material data
        D = pullback_c!(element)

        # compute element stiffness matrices for component directions i ∈ 1:2 and j ∈ 1:2
        # these are embarrisingly parallel
        # symmetry has not been used
        for j in 1:Dim
            for i in 1:Dim
                # get sumfactorization object
                ∫ = Sumfactory(mat_cache, element)
                
                # compute components related to first derivatives of test and trial functions
                for beta in 1:Dim
                    for alpha in 1:Dim    
                        ∫(∇u[beta], ∇v[alpha]; data=D[i,j].data[alpha,beta], reset=false)
                    end
                end

                # add to global stiffness matrix
                K[i,j][A, B] += ∫.data[1]
            
            end  # spatial directions
        end  # spatial directions

    end # element loop

    # loop over boundaries
    for side in 1:2Dim

        # define pull-back of traction vector on current boundary component
        pullback_t!(e) = pullback_traction_vector!(acc, e, FieldEvaluationCache(acc, Dim, 1), boundary(mapping, side), traction)

        # loop over boundary elements
        for element in Elements(restriction(partition, side))
        
            # get global indices
            A = TestIndices(acc, element)

            # compute test function derivatives at the quadrature points
            v = TestFunctions(acc, element; ders=ntuple(k->0, Dim))

            # compute the pulled-back material data
            F_t = pullback_t!(element)

            # loop over the spatial directions
            for i in 1:Dim

                #get sumfactorization object
                ∫ = Sumfactory(vec_cache, element)

                # compute contribution to force vector
                ∫(v; data=F_t.data[i], reset=false)
                
                # add to global stiffness matrix
                f[i][A] += ∫.data[1]

            end # spatial directions

        end # element loop

    end # all boundaries

    # assemble global system matrix
    A, b = assemble_block_matrix_and_rhs(Val(Dim), K, f)

    # apply bcs
    apply_clamped_boundary_conditions!(Val(Dim), A, b, fixed_nodes)

    # return stiffness matrix and forcing vector
    return A, b
end

function assemble_block_matrix_and_rhs(::Val{2}, K, F)
    A = [K[1,1] K[1,2]; 
         K[2,1] K[2,2]]
    b = [F[1]; F[2]]
    return A, b
end

function assemble_block_matrix_and_rhs(::Val{3}, K, F)
    A = [K[1,1] K[1,2] K[1,3]; 
         K[2,1] K[2,2] K[2,3];
         K[3,1] K[3,2] K[3,3]]
    b = [F[1]; F[2]; F[3]]
    return A, b
 end

# apply Dirichlet boundary conditions
function apply_clamped_boundary_conditions!(::Val{Dim}, A, b, indices) where Dim
    m = Int(length(b) / Dim)
    for k in 1:Dim
        for I in indices[k]
            gind = I .+ m*(k-1)
            for a in gind
                A[a,:] .= 0
                A[a,a] = 1
                b[a] = 0
            end
        end
    end
end
    
ders(dim, k::Int) = ntuple(i -> i==k ? 1 : 0, dim)